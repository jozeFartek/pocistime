package com.example;

import java.util.UUID;

/**
 * Created by Joze on 26. 02. 2017.
 */

public class MyLocationTag {
    String ID;
    String idMyLocation;
    String idTag;
    long date;
    String idUser;

    public MyLocationTag(String idMyLocation, String idTag, long date, String idUser) {
        this.ID = UUID.randomUUID().toString().replaceAll("-", "");
        this.idMyLocation = idMyLocation;
        this.idTag = idTag;
        this.date = date;
        this.idUser = idUser;
    }

    @Override
    public String toString() {
        return "MyLocationTag{" +
                "ID='" + ID + '\'' +
                ", idMyLocation='" + idMyLocation + '\'' +
                ", idTag='" + idTag + '\'' +
                ", date=" + date +
                ", idUser='" + idUser + '\'' +
                '}';
    }
}
