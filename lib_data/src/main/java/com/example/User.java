package com.example;

/**
 * Created by Joze on 26. 02. 2017.
 */


public class User {
    private String idUser;
    private String nickName;
    private String password;

    private String image_url;



    public User(String idUser, String nickName, String password) {
        this.idUser = idUser;
        this.nickName = nickName;
        this.password = password;
    }

    public User(String idUser, String nickName, String password, String image_url) {
        this.idUser = idUser;
        this.nickName = nickName;
        this.password = password;
        this.image_url = image_url;
    }

    public String getIdUser() {
        return idUser;
    }

    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    @Override
    public String toString() {
        return "User{" +
                "idUser='" + idUser + '\'' +
                ", nickName='" + nickName + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
