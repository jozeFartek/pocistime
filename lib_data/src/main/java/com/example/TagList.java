package com.example;

import java.util.ArrayList;

/**
 * Created by Joze on 26. 02. 2017.
 */

public class TagList {
    private ArrayList<Tag> list;

    public TagList() {
        list = new ArrayList<>();
    }

    public void Add(Tag t){
        list.add(t);
    }

    public Tag get(int index)
    {
        return list.get(index);
    }

    @Override
    public String toString() {
        return "TagList{" +
                "list=" + list +
                '}';
    }
}
