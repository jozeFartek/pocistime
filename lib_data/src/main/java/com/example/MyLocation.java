package com.example;

import java.util.UUID;

/**
 * Created by Joze on 26. 02. 2017.
 */

public class MyLocation {
    String ID;
    String name;
    double latitude, longitude; //GPS
    String idUser;
    String fileName;
    long date;

    public MyLocation(String name, double latitude, double longitude, String idUser, String fileName, long date) {
        this.ID = UUID.randomUUID().toString().replaceAll("-", "");
        this.name = name;
        this.latitude = latitude;
        this.longitude = longitude;
        this.idUser = idUser;
        this.fileName = fileName;
        this.date = date;
    }



    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getIdUser() {
        return idUser;
    }

    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "MyLocation{" +
                "id='" + ID + '\'' +
                ", name='" + name + '\'' +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                ", idUser='" + idUser + '\'' +
                ", fileName='" + fileName + '\'' +
                ", date=" + date +
                '}';
    }
}
