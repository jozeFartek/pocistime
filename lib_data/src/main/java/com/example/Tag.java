package com.example;

/**
 * Created by Joze on 26. 02. 2017.
 */

public class Tag {
    private String name;

    public Tag(String name) {
        this.name = name;
    }

    public String idTag() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Tag{" +
                "name='" + name + '\'' +
                '}';
    }
}
