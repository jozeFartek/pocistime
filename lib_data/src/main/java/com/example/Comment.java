package com.example;

import java.util.UUID;

/**
 * Created by Joze on 17. 04. 2017.
 */

public class Comment {
    String ID;
    String userName;
    String idMyLocation;
    String content;
    long date;

    public Comment(String userName, String idMyLocation, String content, long date) {
        this.ID = UUID.randomUUID().toString().replaceAll("-", "");
        this.userName = userName;
        this.idMyLocation = idMyLocation;
        this.content = content;
        this.date = date;
    }

    public Comment(String ID, String userName, String idMyLocation, String content, long date) {
        this.ID = ID;
        this.userName = userName;
        this.idMyLocation = idMyLocation;
        this.content = content;
        this.date = date;
    }

    public String getIdMyLocation() {
        return idMyLocation;
    }

    public void setIdMyLocation(String idMyLocation) {
        this.idMyLocation = idMyLocation;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }
}
