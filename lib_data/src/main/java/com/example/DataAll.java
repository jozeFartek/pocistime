package com.example;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;

/**
 * Created by Joze on 26. 02. 2017.
 */

public class DataAll {
    public static final String LOKACIJA_ID = "lokacija_id";
    public static SimpleDateFormat dt = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
    private TagList tags;


    private User userMe;
    //private Hashtable<String, MyLocation> locationList;
    private ArrayList<MyLocation> locationList;
    private ArrayList<MyLocationTag> locationTagList;
    private ArrayList<Comment> commentsList;

    public DataAll() {
        userMe = new User("neznani.nedolocen@nekje.ne","NiDefiniran", "geslo");
        tags = new TagList();
        //locationList = new Hashtable<>();
        locationList = new ArrayList<>();
        locationTagList = new ArrayList<>();
        commentsList = new ArrayList<>();
    }


    @Override
    public String toString() {
        return "DataAll{" +
                "\ntags=" + tags +
                ", \nuserMe=" + userMe +
                ", \nlocationList=" + locationList +
                ", \nlocationTagList=" + locationTagList +
                '}';
    }

    public static DataAll scenarijA(){
        DataAll da = new DataAll();
        Date danes = new Date();
        da.userMe = new User("joze.fartek@student.um.si", "JozeFar", "mojeGeslo123");
        // dodamo Tage
        da.tags.Add(new Tag("Igle"));
        da.tags.Add(new Tag("Plastenke"));
        da.tags.Add(new Tag("Mesto"));
        da.tags.Add(new Tag("Nujno"));
        da.tags.Add(new Tag("Gozd"));
        MyLocation tmp;
        tmp = da.addLocation("Celje, Robova ulica 11", 46.243429,15.260077, "");
        da.addLocationTag(tmp, da.tags.get(0));
        da.addLocationTag(tmp, da.tags.get(1));
        tmp = da.addLocation("Maribor, Smetanova ulica 67", 46.5610827,15.631160399, "");
        da.addLocationTag(tmp, da.tags.get(1));
        da.addLocationTag(tmp, da.tags.get(2));
        tmp = da.addLocation("Pravna fakulteta Maribor", 46.563015008,15.645790815, "");
        da.addLocationTag(tmp, da.tags.get(3));

        // KOMENTARJI
        da.addComment(tmp.getID(), "Lorem Ipsum is simply dummy text of the printing and typesetting industry. ");
        da.addComment(tmp.getID(), "Lorem Ipsum is simply dummy text of the printing and typesetting industry. ");
        da.addComment(tmp.getID(), "Lorem Ipsum is simply dummy text of the printing and typesetting industry. ");

        for( int i = 1; i < 3; i++){
            tmp = da.addLocation(("Maribor, Gosposvetska ulica " + i) , i, i+1, "");
        }




        return da;
    }


    public void setUserMe(User userMe) {
        this.userMe = userMe;
    }

    /*
    public MyLocation getLocation(int i) {
        return locationList.get(locationList.keys().nextElement());
    }

    public List<MyLocation> getLokacijaAll() {
        ArrayList<MyLocation> l = new ArrayList<>();
        l.addAll(locationList.values());
        return  l;
    }
    public MyLocation getLocationByID(String ID) {
        return locationList.get(ID);
    }
     public List<String> getLocationIDs() {
        return  Collections.list(locationList.keys());
    }

     */

    public MyLocation removeLocation(String ID){
        for (MyLocation l: locationList) { //TODO this solution is relatively slow! If possible don't use it!
            if (l.getID().equals(ID)){
                locationList.remove(l);
                return l;
            }
        }
        return null;
    }

    public MyLocation getLocation(int i) {
        return locationList.get(i);
    }

    public List<MyLocation> getLocationAll() {
        return locationList;
    }

    public MyLocation getLocationByID(String ID) {
        for (MyLocation l: locationList) { //TODO this solution is relatively slow! If possible don't use it!
            if (l.getID().equals(ID)) return l;
        }
        return null;
    }

    public int getLocationSize() {
        return locationList.size();
    }

    public MyLocation getNewLocation(double latitude, double longitude) {
        return addLocation("N/A", latitude, longitude, "");
    }

    public void addLocation(MyLocation l) {
        locationList.add(l);
    }

    public MyLocation addLocation(String name, double la, double lt, String img) {
        MyLocation tmp = new MyLocation(name, la, lt, userMe.getIdUser(), img, System.currentTimeMillis());
        //locationList.put(tmp.getID(), tmp);
        locationList.add(tmp);
        return tmp;
    }

    public void addLocationAt(MyLocation l, int index){
        locationList.add(index, l);
    }

    public void addLocationTag(MyLocation l, Tag t){
        locationTagList.add(new MyLocationTag(l.ID, t.idTag(), System.currentTimeMillis(),userMe.getIdUser()));
    }
    public User getUserMe() {
        return userMe;
    }


    public void addComment(String idMyLocation, String content) {
        commentsList.add(new Comment(userMe.getNickName(), idMyLocation, content, System.currentTimeMillis()));
    }

    public Comment getComment(int index, String idLoc) {
        ArrayList<Comment> tmp = new ArrayList<Comment>();
        for(int i=0; i<commentsList.size(); i++)
        {
            if((commentsList.get(i).getIdMyLocation().equals(idLoc)))
                tmp.add(commentsList.get(i));
        }
        return tmp.get(index);
    }

    public List<Comment> getCommentAll(String idLoc)
    {
        ArrayList<Comment> tmp = new ArrayList<Comment>();
        for(int i=0; i<commentsList.size(); i++)
        {
            if((commentsList.get(i).getIdMyLocation().equals(idLoc)))
                tmp.add(commentsList.get(i));
        }
        return tmp;
    }

    public int getCommentSize(String idLoc) {
        ArrayList<Comment> tmp = new ArrayList<Comment>();
        for(int i=0; i<commentsList.size(); i++)
        {
            if((commentsList.get(i).getIdMyLocation().equals(idLoc)))
                tmp.add(commentsList.get(i));
        }
        return tmp.size();
    }


}
