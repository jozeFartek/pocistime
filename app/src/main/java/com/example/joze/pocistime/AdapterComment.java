package com.example.joze.pocistime;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.Comment;
import com.example.DataAll;
import com.example.User;

/**
 * Created by Joze on 18. 04. 2017.
 */

public class AdapterComment extends RecyclerView.Adapter<AdapterComment.ViewHolder> {

    DataAll all;
    Activity ac;
    Context context;
    String idLoc;

    public AdapterComment(DataAll all, Activity ac, String idLoc) {
        super();
        this.all = all;
        this.ac = ac;
        this.idLoc = idLoc;
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView tvUser;
        public TextView tvContent;

        public ViewHolder(View v) {
            super(v);
            tvUser = (TextView) v.findViewById(R.id.rowlayout_comment_tv_user);
            tvContent = (TextView) v.findViewById(R.id.rowlayout_comment_tv_content);
        }
    }

    @Override
    public AdapterComment.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.rowlayout_comment, parent, false);
        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(v);

        return vh;
    }

    @Override
    public void onBindViewHolder(AdapterComment.ViewHolder holder, int position) {
        final Comment comment = all. getComment(position, idLoc);
        final User user =  all.getUserMe();
        final String content = comment.getContent();
        holder.tvUser.setText(comment.getUserName());
        holder.tvContent.setText(content);
    }

    @Override
    public int getItemCount() {
        return all.getCommentSize(idLoc);
    }
}
