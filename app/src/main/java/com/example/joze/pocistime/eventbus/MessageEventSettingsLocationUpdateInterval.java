package com.example.joze.pocistime.eventbus;

/**
 * Created by crepinsek on 14/04/17.
 */

public class MessageEventSettingsLocationUpdateInterval {
    int interval;

    public int getInterval() {
        return interval;
    }

    public MessageEventSettingsLocationUpdateInterval(int interval) {

        this.interval = interval;
    }
}
