package com.example.joze.pocistime;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.app.ListActivity;
import android.view.View.OnClickListener;

import com.example.DataAll;
import com.example.MyLocation;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import com.example.joze.pocistime.eventbus.MessageEventUpdateLocation;
/*import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
*/
import java.util.List;

public class ActivityMain extends AppCompatActivity {
//    ApplicationMy app;
//    private RecyclerView mRecyclerView;
//    private AdapterMyLocation mAdapter;
//    private RecyclerView.LayoutManager mLayoutManager;
//    private FloatingActionButton fab;
//
//   // private LocationUpdateReceiver dataUpdateReceiver;
//    private Location mLocation;
//
//    /*
//    private class LocationUpdateReceiver extends BroadcastReceiver {
//
//        @Override
//        public void onReceive(Context context, Intent intent) {
//            if (intent.getAction().equals(GPSTracker.GPSTrackerEvent)) {
//                mLocation = intent.getParcelableExtra(GPSTracker.GPSTrackerKeyLocation);
//                app.setLastLocation(mLocation);
//                System.out.println("Ali dela "+ System.currentTimeMillis()+" "+mLocation.toString());
//            }
//        }
//    }
//    */
//
//    @Subscribe(threadMode = ThreadMode.MAIN)
//    public void onMessageEvent(MessageEventUpdateLocation event) {
//        Log.i("ActivityZacetna","MessageEventUpdateLocation ");
//        mLocation = event.getM();
//        mAdapter.setLastLocation(mLocation);
//        fab.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorPrimary)));
//    }
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_main);
//        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);
//        app = (ApplicationMy) getApplication();
//
//
//        mRecyclerView = (RecyclerView) findViewById(R.id.myRecycleView);
//        // use this setting to improve performance if you know that changes
//        // in content do not change the layout size of the RecyclerView
//        mRecyclerView.setHasFixedSize(true);
//
//        // use a linear layout manager
//        mLayoutManager = new LinearLayoutManager(this);
//        mRecyclerView.setLayoutManager(mLayoutManager);
//        // specify an adapter (see also next example)
//        app = (ApplicationMy) getApplication();
//        mAdapter = new AdapterMyLocation(app.getAll(), this);
//        mRecyclerView.setAdapter(mAdapter);
//
//        fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent i = new Intent(getBaseContext(), ActivityLocation.class);
//                i.putExtra(DataAll.LOKACIJA_ID, ActivityLocation.NEW_LOCATION_ID);
//                startActivity(i);
//            }
//        });
//
//
//        // Setup ItemTouchHelper
//        ItemTouchHelper.Callback callback = new ItemTouchHelperMyLocation(mAdapter, mRecyclerView);
//        ItemTouchHelper helper = new ItemTouchHelper(callback);
//        helper.attachToRecyclerView(mRecyclerView);
//
//
//        // APP PERMISIONS
//    //    ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
//    //    ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, 1);
//    }
//
//
//    @Override
//    protected void onResume() {
//        super.onResume();
//        if (mLocation==null) {
//
//            fab.setBackgroundColor(Color.BLUE);
//        }
//        if (!EventBus.getDefault().isRegistered(this)) EventBus.getDefault().register(this);
//        //  checkPermissions();
//        app.sortUpdate();
//        mAdapter.notifyDataSetChanged();
//
//        startService(new Intent(app, GPSTracker.class));//start service
//
//        fab.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.accent)));
//
//
//        /*mAdapter.notifyDataSetChanged();
//        if (dataUpdateReceiver == null) dataUpdateReceiver = new LocationUpdateReceiver();
//        IntentFilter intentFilter = new IntentFilter(GPSTracker.GPSTrackerEvent);
//        registerReceiver(dataUpdateReceiver, intentFilter);
//        startService(new Intent(app, GPSTracker.class));//start service
//        */
//    }
//
//    // TOOLBAR
//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.my_menu, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        switch (item.getItemId()) {
//            case R.id.action_settings:
//                // User chose the "Settings" item, show the app settings UI...
//                startActivity(new Intent(this,ActivityMySettings.class));
//                return true;
//
//            case R.id.action_sort:
//                //app.sortUpdate();
//                app.sortChangeAndUpdate();
//                mAdapter.notifyDataSetChanged();
//                return true;
//
//            default:
//                return super.onOptionsItemSelected(item);
//
//        }
//    }
//
//    public ApplicationMy getApp() {
//        return app;
//    }
//
//    public void setApp(ApplicationMy app) {
//        this.app = app;
//    }
//    /*
//    public void onClickOpen(View view){
//        Intent i = new Intent(getBaseContext(), ActivityLocation.class);
//        i.putExtra(DataAll.LOKACIJA_ID, ((MyLocation)mySpinner.getSelectedItem()).getID());
//        startActivity(i);
//    }
//    */
//
///*
//    private void getPermissions() {
//        MultiplePermissionsListener my  = new MultiplePermissionsListener() {
//            @Override public void onPermissionsChecked(MultiplePermissionsReport report) {
//                if (!report.areAllPermissionsGranted()) {
//                    new android.app.AlertDialog.Builder(ActivityMain.this)
//                            .setTitle(getString(R.string.permission_must_title))
//                            .setMessage(getString(R.string.permission_must))
//                            .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
//                                @Override public void onClick(DialogInterface dialog, int which) {
//                                    dialog.dismiss();
//                                    ActivityMain.this.finish(); //end app!
//                                }
//                            })
//                            .setIcon(R.mipmap.ic_launcher)
//                            .show();
//                }}
//            @Override public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
//                token.continuePermissionRequest();
//            }
//        };
//
//
//        Dexter.withActivity(this)
//                .withPermissions(
//                        Manifest.permission.READ_EXTERNAL_STORAGE,
//                        Manifest.permission.ACCESS_FINE_LOCATION,
//                        Manifest.permission.ACCESS_COARSE_LOCATION,
//                        Manifest.permission.CAMERA
//                ).withListener(my).check();
//    }
//*/
//    @Override
//    protected void onStart() {
//        super.onStart();
//      //  getPermissions();
//    }
//
//    @Override
//    protected void onPause() {
//        super.onPause();
//        //  if (dataUpdateReceiver != null) unregisterReceiver(dataUpdateReceiver);
//    }
//
//    @Override
//    protected void onStop() {
//        EventBus.getDefault().unregister(this);
//
//        super.onStop();
//    }
//
//    @Override
//    protected void onDestroy() {
//        super.onDestroy();
//     //   stopService(new Intent(ActivityMain.this, GPSTracker.class));
//    }
//
//    public Location getLocation() {
//        return mLocation;
//    }
}
