package com.example.joze.pocistime;

import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;

import com.example.DataAll;

import static android.support.v7.widget.helper.ItemTouchHelper.*;

/**
 * Created by Joze on 26. 03. 2017.
 */

public class ItemTouchHelperMyLocation extends SimpleCallback {
    private AdapterMyLocation adapterMyLocation;
    RecyclerView recyclerView;

    public ItemTouchHelperMyLocation(AdapterMyLocation adapterMyLocation, RecyclerView recyclerView) {
        super(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT);
        this.adapterMyLocation = adapterMyLocation;
        this.recyclerView = recyclerView;
    }

    @Override
    public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
        return false;
    }

    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
        adapterMyLocation.remove(viewHolder.getAdapterPosition(), recyclerView);
    }
}
