package com.example.joze.pocistime;

import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.DataAll;
import com.example.joze.pocistime.eventbus.MessageEventUpdateLocation;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;


public class FragmentList extends Fragment {

    ApplicationMy app;
    ActivityMain2 activityMain2;

    private RecyclerView mRecyclerView;
    private AdapterMyLocation mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private FloatingActionButton fab;
    private boolean hasLoaction;

    // private LocationUpdateReceiver dataUpdateReceiver;
    private Location mLocation;


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MessageEventUpdateLocation event) {
        Log.i("Fragment_map","MessageEventUpdateLocation ");
        mLocation = event.getM();
        hasLoaction = true;
        mAdapter.setLastLocation(mLocation);
    }
    public static FragmentList newInstance() {
        FragmentList fragment = new FragmentList();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_list, container, false);

        mRecyclerView = (RecyclerView) view.findViewById(R.id.myRecycleView);
        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(getActivity());

        mRecyclerView.setLayoutManager(mLayoutManager);
        // specify an adapter (see also next example)
        app = (ApplicationMy) getActivity().getApplication();
        //mAdapter = new AdapterMyLocation(app.getAll(), this);
        mAdapter = new AdapterMyLocation(app.getAll(), (ActivityMain2) getActivity());
        mRecyclerView.setAdapter(mAdapter);

        // Setup ItemTouchHelper
        ItemTouchHelper.Callback callback = new ItemTouchHelperMyLocation(mAdapter, mRecyclerView);
        ItemTouchHelper helper = new ItemTouchHelper(callback);
        helper.attachToRecyclerView(mRecyclerView);

        fab = (FloatingActionButton) view.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(hasLoaction){
                    Intent i = new Intent(getActivity().getBaseContext(), ActivityLocation.class);
                    i.putExtra(DataAll.LOKACIJA_ID, ActivityLocation.NEW_LOCATION_ID);
                    startActivity(i);
                }
                else
                {
                    Snackbar snackbar = Snackbar
                            .make(getView(), "Ni pridobljene lokacije.", Snackbar.LENGTH_LONG);
                    snackbar.show();
                }
            }
        });

        fab = (FloatingActionButton) view.findViewById(R.id.fabSort);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                app.sortChangeAndUpdate();
                mAdapter.notifyDataSetChanged();
            }
        });

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!EventBus.getDefault().isRegistered(this)) EventBus.getDefault().register(this);
        //  checkPermissions();
        app.sortUpdate();
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    public ApplicationMy getApp() {
        return app;
    }

    public void setApp(ApplicationMy app) {
        this.app = app;
    }
}