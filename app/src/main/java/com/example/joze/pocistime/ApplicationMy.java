package com.example.joze.pocistime;

import android.app.Application;
import android.content.SharedPreferences;
import android.location.Location;
import android.preference.PreferenceManager;
import android.util.Log;

import com.example.DataAll;
import com.example.MyLocation;


import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import android.util.Log;
import com.example.joze.pocistime.eventbus.MessageEventUpdateLocation;


/**
 * Created by Joze on 1. 03. 2017.
 */

public class ApplicationMy extends Application{
    public static SharedPreferences preferences;
    private static final String DATA_MAP = "pocistiMedatamap";
    private static final String FILE_NAME = "pocistiMe.json";
    private Location mLastLocation;
    DataAll dataAll;



    private boolean LogInStatus = false;

    private static final int SORT_BY_DATE=0;
    private static final int SORT_BY_DISTACE=1;
    int sortType = SORT_BY_DATE;

    @Override
    public void onCreate() {
        super.onCreate();
        EventBus.getDefault().register(this);
        preferences = PreferenceManager.getDefaultSharedPreferences(this);
        mLastLocation=null;
        if (!load())
            dataAll = DataAll.scenarijA();
    }


    @Subscribe
    public void onMessageEvent(MessageEventUpdateLocation event) {
        Log.i("ApplicationMy","MessageEventUpdateLocation ");
        mLastLocation = event.getM();
    }
    @Override
    public void onTerminate() {
        EventBus.getDefault().unregister(this);
        super.onTerminate();

    }
    public DataAll getAll() {
        return  dataAll;
    }

    public MyLocation RemoveMyLocation(String id){
        return dataAll.removeLocation(id);
    }

    public void addLocationAt(MyLocation l, int index) {
        dataAll.addLocationAt(l, index);
    }

    public MyLocation getTestLocation() {
        return dataAll.getLocation(0);
    }

    public MyLocation getLocationByID(String id) {
        return dataAll.getLocationByID(id);
    }

    public MyLocation getListLocationID(String id) {
        return dataAll.getLocationByID(id);
    }
    public List<MyLocation> getLocationAll() {
        return dataAll.getLocationAll();
    }

    public MyLocation getNewLocation(double latitude, double longitude) {
        return dataAll.getNewLocation(latitude,longitude);
    }

    public boolean save() {
        File file = new File(this.getExternalFilesDir(DATA_MAP), ""
                + FILE_NAME);

        return ApplicationJson.save(dataAll,file);
    }
    public boolean load(){
        File file = new File(this.getExternalFilesDir(DATA_MAP), ""
                + FILE_NAME);
        DataAll tmp = ApplicationJson.load(file);
        if (tmp!=null) dataAll = tmp;
        else return false;
        return true;
    }

    public boolean isLogInStatus() {
        return LogInStatus;
    }

    public void setLogInStatus(boolean logInStatus) {
        LogInStatus = logInStatus;
    }


    // LOCATION
    public Location getLastLocation() {
        return mLastLocation;
    }

    public void setLastLocation(Location mLastLocation) {
        this.mLastLocation = mLastLocation;
    }
    public boolean hasLocation() {
        if (mLastLocation==null) return false;
        return true;
    }


    public void sortUpdate() {
        //sortType= (sortType+1) / 2;
        switch (sortType) {
            case SORT_BY_DATE:{
                Collections.sort(dataAll.getLocationAll(), new Comparator<MyLocation>() {
                    @Override
                    public int compare(MyLocation l1, MyLocation l2) {
                        if (l1.getDate()==l2.getDate()) return 0;
                        if (l1.getDate()>l2.getDate()) return -1;
                        return 1;
                    }
                });
            }
            break;
            case SORT_BY_DISTACE:{
                if (mLastLocation==null) return;
                Collections.sort(dataAll.getLocationAll(), new Comparator<MyLocation>() {
                    @Override
                    public int compare(MyLocation l1, MyLocation l2) {
                        int d1 = Util.distance(mLastLocation.getLatitude(),mLastLocation.getLongitude(),l1.getLatitude(),l1.getLongitude());
                        int d2 = Util.distance(mLastLocation.getLatitude(),mLastLocation.getLongitude(),l2.getLatitude(),l2.getLongitude());
                        if (d1==d2) return 0;
                        if (d1>d2) return 1;
                        return -1;
                    }
                });

            }
            break;
        }

    }
    public void sortChangeAndUpdate() {
        sortType= (sortType+1) % 2;
        sortUpdate();
    }

}
