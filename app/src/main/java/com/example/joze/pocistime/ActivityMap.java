package com.example.joze.pocistime;


import android.content.Context;
import android.graphics.Bitmap;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.DataAll;
import com.example.MyLocation;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class ActivityMap extends AppCompatActivity implements OnMapReadyCallback {

    private ApplicationMy app;
    private BottomSheetBehavior mBottomSheetBehavior;
    private FloatingActionButton fab;
    private TextView tv_myLocation_name;
    private TextView tv_myLocation_status;
    private TextView tv_myLocation_time;
    private ImageView img_myLocation;

    private MyLocation location;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
       setSupportActionBar(toolbar);
        app = (ApplicationMy) getApplication();

        tv_myLocation_name = (TextView) findViewById(R.id.textViewName);
        tv_myLocation_time = (TextView) findViewById(R.id.textViewTime);
        tv_myLocation_status = (TextView) findViewById(R.id.textViewStatus);
        img_myLocation = (ImageView) findViewById(R.id.bs_image);


        /**** google map ****/
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });


        View bottomSheet = findViewById(R.id.bottom_sheet);
        mBottomSheetBehavior = BottomSheetBehavior.from(bottomSheet);

        // set callback for changes
        mBottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                if (BottomSheetBehavior.STATE_COLLAPSED == newState) {
                    fab.show();
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
            }
        });


    }

    // GOOGLE MAP
    @Override
    public void onMapReady(GoogleMap map) {
        LatLng coordinate = new LatLng(app.getTestLocation().getLatitude(), app.getTestLocation().getLongitude());
        map.addMarker(new MarkerOptions().position(coordinate).title("Marker").icon(BitmapDescriptorFactory.fromResource(R.drawable.marker)));

        coordinate = new LatLng(app.getTestLocation().getLatitude() + 0.001, app.getTestLocation().getLongitude() + 0.001);
        map.addMarker(new MarkerOptions().position(coordinate).title("Marker").icon(BitmapDescriptorFactory.fromResource(R.drawable.marker2)));
        CameraUpdate yourLocation = CameraUpdateFactory.newLatLngZoom(coordinate, 15);
        //map.animateCamera(yourLocation);
        map.moveCamera(yourLocation);

        map.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener()
        {
            @Override
            public boolean onMarkerClick(Marker marker) {
                fab.hide();

                updateBottomSheet(app.getTestLocation());
                mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                return true;
            }
        });
    }

    public void updateBottomSheet(MyLocation l) {
        tv_myLocation_name.setText(l.getName());
        tv_myLocation_status.setText("SOLVED");
        tv_myLocation_time.setText(DataAll.dt.format(new Date(l.getDate())));
        // Set picture
        File slika = new File(l.getFileName());
        if (slika.exists()) {
            Picasso.with(this)
                    .load(slika)
                    .fit()
                    .centerCrop()
                    .into(img_myLocation);
        }
    }



}
