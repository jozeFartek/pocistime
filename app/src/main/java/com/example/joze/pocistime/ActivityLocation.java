package com.example.joze.pocistime;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.Address;
import android.location.Geocoder;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.DataAll;
import com.example.MyLocation;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import com.example.joze.pocistime.eventbus.MessageEventUpdateLocation;

public class ActivityLocation extends AppCompatActivity implements OnMapReadyCallback {
    private RecyclerView mRecyclerView;
    private AdapterComment mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private FloatingActionButton fab;

    static final int REQUEST_TAKE_PHOTO = 1;
    public static String NEW_LOCATION_ID = "NEW_LOCATION";
    String mCurrentPhotoPath;
    boolean stateNew;
    ApplicationMy app;
    ImageView ivImage;
    EditText etName;
 //   EditText etLatitude;
 //   EditText etLongitude;
    TextView tvDate;
    Button save;
    MyLocation l;
    String ID;

    private LocationUpdateReceiver dataUpdateReceiver;
    Location mLocation;
    private class LocationUpdateReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(GPSTracker.GPSTrackerEvent)) {
                mLocation = intent.getParcelableExtra(GPSTracker.GPSTrackerKeyLocation);
                app.setLastLocation(mLocation);
              //  l.setLatitude(mLocation.getLatitude());
             //   l.setLongitude(mLocation.getLongitude());
              //  etLatitude.setText("121313");
              //  etLongitude.setText(l.getLongitude()+ "");
                //app.setLastLocation(mLocation);
                System.out.println("Ali TTTdela 2"+ System.currentTimeMillis()+" "+mLocation.toString());
            }
        }
    }


    @Subscribe
    public void onMessageEvent(MessageEventUpdateLocation event) {
        Log.i("ActivityLocation11","MessageEventUpdateLocation ");
        mLocation = event.getM();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location);
        app = (ApplicationMy) getApplication();
        ivImage = (ImageView) findViewById(R.id.ivImage);
        etName = (EditText) findViewById(R.id.etName);
       // etLatitude = (EditText) findViewById(R.id.etLatitude);
      //  etLongitude = (EditText) findViewById(R.id.etLongitude);
        tvDate = (TextView) findViewById(R.id.tvDate);

        // TOOLBAR
        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        // TOOLBAR BACK BUTTON + LISTENER
        myToolbar.setNavigationIcon(R.drawable.ic_arrow_back);
        myToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed(); // Implemented by activity
            }
        });

        /**** google map ****/
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);


        fab = (FloatingActionButton) findViewById(R.id.floatingActionButton4);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(app.isLogInStatus())
                {
                    AddComent();
                    save();
                }
                else
                {
                    Snackbar snackbar1 = Snackbar.make(findViewById(R.id.activity_location) , "Nisi prijavljen!", Snackbar.LENGTH_SHORT);
                    snackbar1.show();
                }


            }
        });
    }


    private void AddComent()
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Napiši komentar");

// Set up the input
        final EditText input = new EditText(this);
// Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
        input.setInputType(InputType.TYPE_CLASS_TEXT);
        builder.setView(input);

// Set up the buttons
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String m_Text = input.getText().toString();
                app.dataAll.addComment(l.getID(), m_Text);
                mAdapter.notifyDataSetChanged();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }
    @Override
    protected void onResume() {
        super.onResume();


        EventBus.getDefault().register(this);
        startService(new Intent(app, GPSTracker.class));//start service


    /*    startService(new Intent(app, GPSTracker.class));//start service
        if (dataUpdateReceiver == null) dataUpdateReceiver = new LocationUpdateReceiver();
        IntentFilter intentFilter = new IntentFilter(GPSTracker.GPSTrackerEvent);
        registerReceiver(dataUpdateReceiver, intentFilter);
*/
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            ID = extras.getString(DataAll.LOKACIJA_ID);
            if (ID.equals(NEW_LOCATION_ID) && !stateNew) {
                fab.hide();
                stateNew = true;
                addNewLocation();
            } else if (!stateNew) {
                stateNew = false;
                setLokacija(extras.getString(DataAll.LOKACIJA_ID));
            }
        } else {
            System.out.println("Nič ni v extras!");
        }


        /**** KOMENTARJI ****/
        mRecyclerView = (RecyclerView) findViewById(R.id.RecycleView_comments);
        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        // specify an adapter (see also next example)
        app = (ApplicationMy) getApplication();
          if(l == null)
             mAdapter = new AdapterComment(app.getAll(), this, "");
         else
        mAdapter = new AdapterComment(app.getAll(), this, l.getID());
        mRecyclerView.setAdapter(mAdapter);



    }


    // TOOLBAR MENU
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_save) {
            if (stateNew) app.getAll().addLocation(l);
            save();
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    // GOOGLE MAP
    @Override
    public void onMapReady(GoogleMap map) {
        if(l != null)
        {
            LatLng coordinate = new LatLng(l.getLatitude(), l.getLongitude());
            map.addMarker(new MarkerOptions().position(coordinate).title("Marker"));

            CameraUpdate yourLocation = CameraUpdateFactory.newLatLngZoom(coordinate, 15);
            //map.animateCamera(yourLocation);
            map.moveCamera(yourLocation);
        }
    }

    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }
            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    void setLokacija(String ID) {
        //Log.d("micka12", l.getLatitude() + "-" + l.getLongitude());
        l = app.getLocationByID(ID);
        update(l);
    }

    public void update(MyLocation l) {
        etName.setText(l.getName());
      //  etLatitude.setText("" + l.getLatitude());
       // etLongitude.setText("" + l.getLongitude());
        tvDate.setText(DataAll.dt.format(new Date(l.getDate())));

        // Set picture
        File slika = new File(l.getFileName());
        if (slika.exists()) {
            Picasso.with(this)
                    .load(slika)
                    .fit()
                    .centerCrop()
                    .into(ivImage);
        } else {
            Picasso.with(this)
                    .load(R.drawable.no_image)
                    .fit()
                    .centerCrop()
                    .into(ivImage);
        }

        // LOCATION
    //    etLatitude.setText(l.getLatitude()+ "");
     //   etLongitude.setText(l.getLongitude()+ "");
    }

    public void save() {
        System.out.println("Prej:" + l);
        l.setName(etName.getText().toString());
      //  l.setLatitude(Double.parseDouble(etLatitude.getText().toString()));
     //   l.setLongitude(Double.parseDouble(etLongitude.getText().toString()));
        System.out.println("Po:" + l);
        app.save();
    }

    public void onClickSaveMe(View v) {
        if (stateNew) app.getAll().addLocation(l);
        save();
        finish();
    }

    // KAMERA
    public void addNewLocation() {
        dispatchTakePictureIntent();
        Log.i("micka123", mCurrentPhotoPath);
        //  l = app.getNewLocation(0,0);
        try{
            if (mLocation!=null)
                l = new MyLocation(getCompleteAddressString(mLocation.getLatitude(), mLocation.getLongitude()), mLocation.getLatitude(), mLocation.getLongitude(), app.getAll().getUserMe().getIdUser(), mCurrentPhotoPath, System.currentTimeMillis());
            else
                // l = new MyLocation(getCompleteAddressString(46.5607902,15.63354), app.getLastLocation().getLatitude(), app.getLastLocation().getLongitude(), app.getAll().getUserMe().getIdUser(), mCurrentPhotoPath, System.currentTimeMillis());
                l = new MyLocation(getCompleteAddressString(app.getLastLocation().getLatitude(), app.getLastLocation().getLongitude()), app.getLastLocation().getLatitude(), app.getLastLocation().getLongitude(), app.getAll().getUserMe().getIdUser(), mCurrentPhotoPath, System.currentTimeMillis());

            update(l);
            Log.i("micka123", l.getLatitude() + " - " + l.getLongitude());
        } catch (Exception e)
        {
            Log.d("Napaka - addNewLoc()", e.getMessage());
        }

    }

    @Override
    protected void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }


    @Override
    protected void onPause() {
        stopService(new Intent(app, GPSTracker.class));//start service
        super.onPause();
    //    if (dataUpdateReceiver != null) unregisterReceiver(dataUpdateReceiver);
    }




    /*
        @Override
        protected void onActivityResult(int requestCode, int resultCode, Intent data) {
            if (requestCode == REQUEST_TAKE_PHOTO && resultCode == RESULT_OK) {
                // Load image
                Bitmap myBitmap = BitmapFactory.decodeFile(l.getFileName());
                ivImage.setImageBitmap(myBitmap);
           }
        }
    */
   /* @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (mLocation!=null)
            l = new MyLocation("Poimenuj", mLocation.getLatitude(), mLocation.getLongitude(), app.getAll().getUserMe().getIdUser(), mCurrentPhotoPath, System.currentTimeMillis());
        else
            l = new MyLocation("Poimenuj", app.getLastLocation().getLatitude(), app.getLastLocation().getLongitude(), app.getAll().getUserMe().getIdUser(), mCurrentPhotoPath, System.currentTimeMillis());
        update(l);

    }
    */

    private String getCompleteAddressString(double LATITUDE, double LONGITUDE) {
        String strAdd = "";
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1);
            if (addresses != null) {
                Address returnedAddress = addresses.get(0);
                StringBuilder strReturnedAddress = new StringBuilder("");
/*
                for (int i = 0; i < returnedAddress.getMaxAddressLineIndex(); i++) {
                    strReturnedAddress.append(returnedAddress.getAddressLine(i)).append(" ");
                }*/
                strReturnedAddress.append(returnedAddress.getAddressLine(0)).append(", ");
                strReturnedAddress.append(returnedAddress.getAddressLine(1));

                strAdd = strReturnedAddress.toString();
                Log.w("My Current loction address", "" + strReturnedAddress.toString());
            } else {
                Log.w("My Current loction address", "No Address returned!");
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.w("My Current loction address", "Canont get Address!");
        }
        return strAdd;
    }


    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
                // save image path, and load image
                // l.setFileName(mCurrentPhotoPath);
            } catch (IOException ex) {
                // Error occurred while creating the File
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(this,
                        "com.example.joze.pocistime",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
            }
        }
    }

}
