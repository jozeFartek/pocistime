package com.example.joze.pocistime;

import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.DataAll;
import com.example.User;
import com.example.joze.pocistime.eventbus.MessageEventUpdateLocation;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;


public class FragmentUser extends Fragment implements View.OnClickListener, GoogleApiClient.OnConnectionFailedListener{
    private ConstraintLayout Prof_Section;
    private  Button SignOut;
    private SignInButton SignIn;
    private TextView Name, Email;
    private ImageView Prof_img;
   // private  GoogleApiClient googleApiClient;
    private static final int REQ_CODE = 9001;

    private ActivityMain2 activityMain2;
    ApplicationMy app;

    public static FragmentUser newInstance() {
        FragmentUser fragment = new FragmentUser();
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        app = (ApplicationMy) getActivity().getApplication();


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_user, container, false);

        Prof_Section = (ConstraintLayout) view.findViewById(R.id.layoutUserInfo);
        SignOut = (Button) view.findViewById(R.id.btnSignOut);
        SignIn = (SignInButton)  view.findViewById(R.id.btnSignIn);
        Name = (TextView) view.findViewById(R.id.textViewUsername);
        Email = (TextView) view.findViewById(R.id.textViewEmail);
        Prof_img = (ImageView) view.findViewById(R.id.imageView_user);

        SignIn.setOnClickListener(this);
        SignOut.setOnClickListener(this);
        Prof_Section.setVisibility(View.GONE);

        activityMain2 = (ActivityMain2) getActivity();
        if( activityMain2.googleApiClient == null || !activityMain2.googleApiClient.isConnected())
        {
            GoogleSignInOptions signInOptions = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestEmail().build();
            activityMain2.googleApiClient = new GoogleApiClient.Builder(getContext()).enableAutoManage(getActivity(), this).addApi(Auth.GOOGLE_SIGN_IN_API, signInOptions).build();
        }
        else
        {
            uppdateUI(true);
        }


        Button btnSettings = (Button) view.findViewById(R.id.btnSettings);
        btnSettings.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity() ,ActivityMySettings.class));
            }
        });

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

    }






    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.btnSignIn:
                signIn();
                break;
            case  R.id.btnSignOut:
                signout();
                break;

        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    private void signIn()
    {
        Intent intent = Auth.GoogleSignInApi.getSignInIntent(activityMain2.googleApiClient);
        startActivityForResult(intent, REQ_CODE);
    }

    private void  signout()
    {
        Auth.GoogleSignInApi.signOut(activityMain2.googleApiClient).setResultCallback(new ResultCallback<Status>() {
            @Override
            public void onResult(@NonNull Status status) {
                uppdateUI(false);
            }
        });
    }

    private void handleResult(GoogleSignInResult result)
    {
        if(result.isSuccess())
        {
            GoogleSignInAccount account = result.getSignInAccount();
            activityMain2.username = account.getDisplayName();
            activityMain2.email = account.getEmail();

            app.dataAll.setUserMe(new User(activityMain2.email, activityMain2.username, ""));
            app.setLogInStatus(true);
            if(account.getPhotoUrl() != null)
            {
                activityMain2.img_url = account.getPhotoUrl().toString();
                app.setLogInStatus(true);
            }


            uppdateUI(true);
        }
        else
        {
            uppdateUI(false);
            app.setLogInStatus(true);
        }
    }

    private void uppdateUI(boolean isLogIn)
    {
        if(isLogIn)
        {
            Prof_Section.setVisibility(View.VISIBLE);
            SignIn.setVisibility(View.GONE);

            // Prikaz podatkov
            if(activityMain2.img_url  != null)
            {
                Picasso.with(getContext())
                        .load(activityMain2.img_url)
                        .fit()
                        .centerCrop()
                        .into(Prof_img);
            }
            Name.setText(activityMain2.username);
            Email.setText(activityMain2.email);
        }
        else
        {
            Prof_Section.setVisibility(View.GONE);
            SignIn.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == REQ_CODE)
        {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleResult(result);
        }
    }
}