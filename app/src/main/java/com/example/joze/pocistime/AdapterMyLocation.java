package com.example.joze.pocistime;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.ShareActionProvider;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.DataAll;
import com.example.MyLocation;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.Date;

/**
 * Created by Joze on 20. 03. 2017.
 */

public class AdapterMyLocation extends RecyclerView.Adapter<AdapterMyLocation.ViewHolder> {
    DataAll all;
    ActivityMain2 ac;
    Context context;



    Location last;
    public static int UPDATE_DISTANCE_IF_DIFF_IN_M=10;

    public void setLastLocation(Location l) {
        if (last==null) {
            last = l;
            notifyDataSetChanged();
        }
        else {
            if (Util.distance(last.getLatitude(),last.getLongitude(),l.getLatitude(),l.getLongitude())>UPDATE_DISTANCE_IF_DIFF_IN_M){
                last = l;
                notifyDataSetChanged();
            }
        }
    }

    public AdapterMyLocation(DataAll all, ActivityMain2 ac) {
        super();
        this.all = all;
        this.ac = ac;
    }



    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView tvTitle;
        public TextView tvDistance;
        public TextView tvTime;
        public ImageView ivImage;
        public ImageView ivShare;
       // public CardView rowLayout;
       public CardView rowLayout3;

        public ViewHolder(View v) {
            super(v);
            /*tvTitle = (TextView) v.findViewById(R.id.tvName);
            tvDistance = (TextView) v.findViewById(R.id.tvDistance);
            rowLayout = (CardView) v.findViewById(R.id.rowLayout);
            */
            tvTitle = (TextView) v.findViewById(R.id.tv_title);
            tvDistance = (TextView) v.findViewById(R.id.tv_distance);
            tvTime = (TextView) v.findViewById(R.id.tv_time);
            ivImage = (ImageView) v.findViewById(R.id.imageViewMyLocation);
            ivShare = (ImageView) v.findViewById(R.id.imageView_share);
            rowLayout3 = (CardView) v.findViewById(R.id.card_row_layout_3);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.rowlayout, parent, false);
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.rowlayout3, parent, false);
        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(v);

        context = parent.getContext();
        return vh;
    }

    private static void startDView(String lokacijaID, Activity ac) {
        //  System.out.println(name+":"+position);
        Intent i = new Intent(ac.getBaseContext(), ActivityLocation.class);
        i.putExtra(DataAll.LOKACIJA_ID,  lokacijaID);
        ac.startActivity(i);

    }
    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final MyLocation location = all.getLocation(position);
        final String name = location.getName();
        holder.tvTitle.setText(name);
        holder.tvDistance.setText(100 + (int)(Math.random() * 2000) + " m");

        if (last==null || ac.getLocation() == null)
            holder.tvDistance.setText("");


        else  holder.tvDistance.setText(Util.getDistanceInString(ac.getLocation().getLatitude(), ac.getLocation().getLongitude(),location.getLatitude(),location.getLongitude()));
        //holder.txtFooter.setText("Footer: " + trenutni.getLatitude()+","+trenutni.getLongitude());
        holder.tvTime.setText(Util.getTimeDiff(System.currentTimeMillis(), location.getDate()));

       // Set picture
        File slika = new File(location.getFileName());
        if(slika.exists()) {
            Picasso.with(context)
                    .load(slika)
                    .fit()
                    .centerCrop()
                    .into(holder.ivImage);
        } else {
            Picasso.with(context)
                    .load(R.drawable.no_image)
                    .fit()
                    .centerCrop()
                    .into(holder.ivImage);
        }


        holder.rowLayout3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AdapterMyLocation.startDView(location.getID(),ac);
            }
        });

        holder.ivShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Share(location);
            }
        });


    }

    public void Share(MyLocation location)
    {
        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");

        String shareString = Html.fromHtml("<p>Naziv: " + location.getName() + "</p>"  +
                                           "<p>Datum: " + DataAll.dt.format(new Date(location.getDate()))+ "</p>" +
                                           "<p>Latitude: " + location.getLatitude() + "</p>" +
                                           "<p>Longitude: " + location.getLongitude() + "</p>"
        ).toString();
        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Onesnažena lokacija");
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareString);

        if (sharingIntent.resolveActivity(context.getPackageManager()) != null)
            context.startActivity(Intent.createChooser(sharingIntent, "Share using"));
        else {
            Toast.makeText(context, "No app found on your phone which can perform this action", Toast.LENGTH_SHORT).show();
        }
    }


    public void remove(final int position, final RecyclerView recyclerView) {
        final MyLocation l = all.removeLocation(all.getLocation(position).getID());

        /* Show SnackBar */
        Snackbar snackbar = Snackbar
                .make(recyclerView, "Lokacija je izbrisana!", Snackbar.LENGTH_LONG)
                .setAction("UNDO", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        // TODO
                        all.addLocationAt(l, position);
                        ( (ApplicationMy)((ActivityMain2)ac).getApp()).save();
                        notifyItemInserted(position);
                        recyclerView.scrollToPosition(position);
                        // Snackbar snackbar1 = Snackbar.make(recyclerView, "Message is restored!", Snackbar.LENGTH_SHORT);
                        // snackbar1.show();
                    }
                });
        snackbar.show();
        ( (ApplicationMy)((ActivityMain2)ac).getApp()).save();
        notifyItemRemoved(position);

    }
    @Override
    public int getItemCount() {
        return all.getLocationSize();
    }

}
