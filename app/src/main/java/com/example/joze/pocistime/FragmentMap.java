package com.example.joze.pocistime;

import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.example.DataAll;
import com.example.MyLocation;
import com.example.joze.pocistime.eventbus.MessageEventUpdateLocation;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;


public class FragmentMap extends Fragment implements OnMapReadyCallback {

    private MapView mapView;
    private GoogleMap googleMap;
    private FloatingActionButton fab;
    private Location mLocation;
    private boolean hasLoaction;


    public static FragmentMap newInstance() {
        FragmentMap fragment = new FragmentMap();
        return fragment;
    }
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MessageEventUpdateLocation event) {
        Log.i("Fragment_map","MessageEventUpdateLocation ");
        mLocation = event.getM();
        hasLoaction = true;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_map, container, false);

        mapView = (MapView) view.findViewById(R.id.map);
        mapView.onCreate(savedInstanceState);
        mapView.onResume();
        mapView.getMapAsync(this);//when you already implement OnMapReadyCallback in your fragment
        hasLoaction = false;

        fab = (FloatingActionButton) view.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(hasLoaction){
                    Intent i = new Intent(getActivity().getBaseContext(), ActivityLocation.class);
                    i.putExtra(DataAll.LOKACIJA_ID, ActivityLocation.NEW_LOCATION_ID);
                    startActivity(i);
                }
                else
                {
                      Snackbar snackbar = Snackbar
                             .make(getView(), "Ni pridobljene lokacije.", Snackbar.LENGTH_LONG);
                             snackbar.show();
                }
            }
        });


        return view;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);



    }


    // GOOGLE MAP
    @Override
    public void onMapReady(GoogleMap map) {
        googleMap = map;

        DataAll dataAll = ((ActivityMain2)getActivity()).getApp().getAll();
        List<MyLocation> list_lokacija = dataAll.getLocationAll();

        for(int i = 0; i < list_lokacija.size(); i++)
        {
            MyLocation myLoc = list_lokacija.get(i);
            LatLng coordinate = new LatLng(myLoc.getLatitude(), myLoc.getLongitude());
            map.addMarker(new MarkerOptions().position(coordinate).title(myLoc.getName()));
        }

      //  MyLocation my_position = ((Act)getActivity())

        LatLng coordinate = new LatLng(46.5608528, 15.6310238);
        map.addMarker(new MarkerOptions().position(coordinate).title("Marker"));

        CameraUpdate yourLocation = CameraUpdateFactory.newLatLngZoom(coordinate, 15);
        //map.animateCamera(yourLocation);
        map.moveCamera(yourLocation);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!EventBus.getDefault().isRegistered(this)) EventBus.getDefault().register(this);
        //  checkPermissions();
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }
}


